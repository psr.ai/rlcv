from ray.rllib.algorithms.dqn import DQNConfig
import ray
from ray import tune
from ray.train import CheckpointConfig, RunConfig
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Train a DQN model on a given environment")
    parser.add_argument("--run-name", type=str, help="Name of the experiment", required=True)
    args = parser.parse_args()
    ray.init()
    config = (  # 1. Configure the algorithm,
        DQNConfig()
        .environment("Taxi-v3")
        .rollouts(num_rollout_workers=8)
        .framework("torch")
        .training(model={"fcnet_hiddens": [1024, 512, 128]})
        .resources(num_gpus_per_worker=0.1)
    )

    config.train_batch_size = 1000
    config.min_train_timesteps_per_iteration = 1500
    config.min_sample_timesteps_per_iteration = 1500

    tuner = tune.Tuner(
            "DQN",
            param_space=config.to_dict(),
            run_config=RunConfig(
                storage_path='/home/psr/ray-results', 
                name=args.run_name,
                checkpoint_config=CheckpointConfig(num_to_keep=1, checkpoint_frequency=2),
                stop={}),
            )

    result = tuner.fit()
