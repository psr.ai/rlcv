#!/usr/bin/env python
from __future__ import print_function

import argparse

import ray
from ray import tune
from ray.rllib.algorithms.algorithm import Algorithm
import gymnasium as gym
from ray.rllib.algorithms.dqn import DQNConfig

def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument("-c", "--configuration-file",
                           help="Configuration file (*.yaml)")
    argparser.add_argument(
        "-ck",
        "--checkpoint",
        type=str,
        help="Checkpoint from which to roll out.", required=True)
    argparser.add_argument("--algo", type=str, help="Algorithm to use.", required=True)
    argparser.add_argument("--num-episodes", type=int, help="number of episodes to run.", default=10)

    args = argparser.parse_args()
    
    ray.init(local_mode=True)
    tuner = tune.Tuner.restore(path=args.checkpoint, trainable=args.algo)
    results = tuner.get_results()
    best_result = results.get_best_result('episode_reward', 'max')
    print("Best result: ", best_result)
    checkpoint = best_result.checkpoint
    assert checkpoint is not None
    algo = Algorithm.from_checkpoint(checkpoint.path)
    
    config = DQNConfig().from_dict(algo.config.to_dict())
    config.explore = False
    config.num_rollout_workers = 1
    config.num_gpus_per_worker = 0.1
    
    algo = config.build()
    algo.restore(checkpoint.path)

    env = gym.make('Taxi-v3', render_mode='human')
    obs, info = env.reset()
    num_episodes = 0
    timestep = 0
    episode_reward = 0.0
    prev_action = None
    reward = 0.0

    while num_episodes < args.num_episodes:
        # Compute an action (`a`).
        a = algo.compute_single_action(observation=obs)
        print(f"Action: {a}")
        timestep += 1
        prev_action = a
        # Send the computed action `a` to the env.
        obs, reward, done, truncated, _ = env.step(a)
        reward = float(reward)
        episode_reward += reward
        print(f"Reward: {reward}, Total reward: {episode_reward}")
        # Is the episode `done`? -> Reset.
        #image = post_process_image(env.core.spectator.get_image())
        #cv2.imshow('birdview', image)
        if done:
            print(f"Episode done: Total reward = {episode_reward}")
            obs, info = env.reset()
            num_episodes += 1
            episode_reward = 0.0
            timestep = 0

    algo.stop()

if __name__ == "__main__":

    main()
