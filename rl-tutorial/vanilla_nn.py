import torch
from torch import nn

class MyModel(nn.Module):
    def __init__(self):
        super(MyModel, self).__init__()
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.create_model()
    
    def create_model(self):
        self.model = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=64, kernel_size=9, stride=1, padding=1), # out dimension = (in_dim - kernel_size + 2*padding)/stride + 1 = (96 - 9 + 2*1)/1 + 1 = 90
            nn.MaxPool2d(kernel_size=2, stride=2), # out dimension = (90 - 2)/2 + 1 = 45
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=128, kernel_size=10, stride=1, padding=1), # out dimension = (45 - 10 + 2*1)/1 + 1 = 38
            nn.MaxPool2d(kernel_size=2, stride=2), # out dimension = (38 - 2)/2 + 1 = 19
            nn.ReLU(),
            nn.Conv2d(in_channels=128, out_channels=256, kernel_size=10, stride=1, padding=1), # out dimension = (19 - 10 + 2*1)/1 + 1 = 12
            nn.MaxPool2d(kernel_size=2, stride=2), # out dimension = (12 - 2)/2 + 1 = 6
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(256*6*6, 2048),
            nn.ReLU(),
            nn.Linear(2048, 1024),
            nn.ReLU(),
            nn.Linear(1024, 64),
            nn.ReLU(),
            nn.Linear(64, 5)
        )
        self.model.to(self.device)
    
    def forward(self, x):
        # Assuming x is in shape (batch_size, height, width, channels)
        # We permute it to (batch_size, channels, height, width)
        x = x.permute(0, 3, 1, 2)  # Change this line based on actual dimension order in your data
        return self.model(x)
    
    def save(self, path):
        torch.save(self.model.state_dict(), path)
