import gymnasium as gym
import pygame, time
from agent import DQNAgent
from torch.utils.tensorboard import SummaryWriter
import argparse
import torch


pygame.display.init()


def run(args):
    
    env = gym.make("CarRacing-v2", domain_randomize=True, continuous=False, render_mode=args.render_mode)
    observation, _ = env.reset(options={"randomize": True})
    episodes = 5_00_000
    current_episode = 0
    print_every = 1000
    replay_every = 10
    decay_every = 20000

    epsilon_start = args.e if args.learn else 0.0
    epsilon_end = 0.001

    decay_factor = args.decay
    print("Decay factor", decay_factor)
    last_interaction_time = None

    should_save_model = True if args.save else False

    gamma = 0.95

    my_agent = DQNAgent(env.observation_space.shape, env.action_space.n, gamma, 1000, 64, epsilon=epsilon_start, ed_min=epsilon_end)
    if args.preload:
        my_agent.model.model.load_state_dict(torch.load(args.preload))
    writer = SummaryWriter(f'runs/dqn-{time.time()}')

    start_time = time.time()
    total_iterations = 0



    while current_episode < episodes:
        episode_reward = 0
        iteration = 0
        while True:
            action = my_agent.act(observation)
            observation_prime, reward, terminated, truncated, info = env.step(action)
            if reward:
                episode_reward += float(reward)
            my_agent.observe(observation, action, reward, observation_prime, terminated)
            if args.learn and iteration % replay_every == 0:
                my_agent.replay()
            
            if args.learn and total_iterations % decay_every == 0:
                my_agent.decay_epsilon(decay_factor)
            observation = observation_prime
            if iteration % print_every == 0:
                last_interaction_time = time.time() - start_time
                print(f"Episode {current_episode}, iteration {iteration}, last loss: {my_agent.loss}, reward: {episode_reward}, current epsilon: {my_agent.epsilon}, last_training_time={my_agent.last_training_time}, time per {print_every} iterations: {last_interaction_time}")
                writer.add_scalar("Epsilon", my_agent.epsilon, total_iterations)
                start_time = time.time()
            if terminated:
                print(f"Episode {current_episode} terminated, loss: {my_agent.loss}, reward: {episode_reward}")
                writer.add_scalar("Reward", episode_reward, current_episode)
                if should_save_model:
                    my_agent.model.save(args.save)
                break
            iteration += 1
            total_iterations += 1
        current_episode += 1
        observation, _ = env.reset(options={"randomize": True})


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--learn", action=argparse.BooleanOptionalAction, help="Should Learn", required=True)
    parser.add_argument("--preload", type=str, help="Model to load")
    parser.add_argument("--save", type=str, help="Model to load", required=False)
    parser.add_argument("--render-mode", type=str, help="Model to load", required=False, default='rgb_array')
    parser.add_argument("--e", type=float, help="start epsilon", required=False, default=1.0)
    parser.add_argument("--decay", type=float, help="epsilon decay", required=False, default=0.995)
    
    args = parser.parse_args()
    assert args.e >= 0.0 and args.e <= 1.0
    run(args)
