from torch import nn
import torch, random
from vanilla_nn import MyModel
import numpy as np
import time

def prod(val) : 
    res = 1
    for ele in val: 
        res *= ele 
    return res

class Memory:
    def __init__(self, memory_size, dtype):
        self.memory = []
        self.memory_size = memory_size
        self.dtype = dtype

    def add(self, v):
        self.memory.append(v)
        if len(self.memory) > self.memory_size:
            self.memory.pop(0)

    def sample(self, indices):
        return np.array(self.memory)[indices]

    def __len__(self):
        return len(self.memory)


class DQNMemory:

    def __init__(self, memory_size):
        self.m_state = Memory(memory_size, dtype=np.float32)
        self.m_action = Memory(memory_size, dtype=np.int32)
        self.m_reward = Memory(memory_size, dtype=np.float32)
        self.m_state_prime = Memory(memory_size, dtype=np.float32)
        self.m_done = Memory(memory_size, dtype=np.bool_)

    def add(self, state, action, reward, state_prime, done):
        self.m_state.add(state)
        self.m_action.add(action)
        self.m_reward.add(reward)
        self.m_state_prime.add(state_prime)
        self.m_done.add(done)

    def sample(self, batch_size):
        indices = np.random.choice(len(self.m_state), batch_size)
        return {
            "state": self.m_state.sample(indices),
            "action": self.m_action.sample(indices),
            "reward": self.m_reward.sample(indices),
            "state_prime": self.m_state_prime.sample(indices),
            "done": self.m_done.sample(indices)
        }

    def __len__(self):
        return len(self.m_state)

class DQNAgent:

    def __init__(self, state_shape, action_space, gamma, memory_size, batch_size, epsilon=0.7, ed_min=0.01, buffer_size=500):

        self.state_shape = state_shape
        self.action_space = action_space
        self.gamma = gamma
        self.epsilon = epsilon
        self.model = MyModel()
        self.memory = DQNMemory(memory_size)
        self.memory_size = memory_size
        self.batch_size = batch_size
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.loss_fn = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=0.01)
        self.loss = None
        self.ed_min = ed_min
        self.buffer_size = buffer_size
        self.last_training_time = None
        print("Using device: ", self.device)

    def replay(self):
        if len(self.memory) < self.buffer_size:
            return
        start_time = time.time()
        # Sample a batch from the memory
        batch = self.memory.sample(self.batch_size)
        not_done_indices = np.where(batch["done"] == False)[0]

        # Ensure arrays are contiguous in memory by copying if necessary
        states_prime = torch.tensor(batch["state_prime"], dtype=torch.float32, device=self.device)
        states = torch.tensor(batch["state"], dtype=torch.float32, device=self.device)
        rewards = torch.tensor(batch["reward"], dtype=torch.float32, device=self.device)
        actions = torch.tensor(batch["action"], dtype=torch.long, device=self.device)

        if len(not_done_indices) > 0:
            not_done_states = states_prime[not_done_indices]
            q_values_next = self.model(not_done_states).detach()
            rewards[not_done_indices] += self.gamma * torch.max(q_values_next, dim=1)[0]

        # Get the Q values of the entire batch
        q_values = self.model(states)

        # Clone Q values to create a target variable for backpropagation
        q_values_pred = q_values.clone()
        q_values_pred[torch.arange(self.batch_size), actions] = rewards

        # Calculate the loss
        self.loss = self.loss_fn(q_values, q_values_pred)

        self.optimizer.zero_grad()
        self.loss.backward()
        self.optimizer.step()
        self.last_training_time = time.time() - start_time

    def observe(self, observation, action, reward, next_observation, done):
        self.memory.add(observation, action, reward, next_observation, done)

    def act(self, state):
        if random.random() < self.epsilon:
            return random.randint(0, self.action_space - 1)

        state = torch.tensor(state, dtype=torch.float32)
        state = state.to(self.device)
        state = torch.unsqueeze(state, 0)
        return torch.argmax(self.model(state)[0]).item()

    def decay_epsilon(self, decay_by = 0.995):
        self.epsilon = max(self.epsilon * decay_by, self.ed_min)
