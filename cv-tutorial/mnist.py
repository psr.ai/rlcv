import torch, torchvision
from torchvision import datasets, transforms, utils
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from VanillaCNN import VanillaCNN, VanillaCNNClass, CustomResNet
from torch.optim import SGD
from torch.nn import CrossEntropyLoss
import argparse, logging, sys

BATCH_SIZE = 32
EPOCHS = 10
LEARNING_RATE = 0.01

model_path = 'models'
data_path = '~/data/torchvision/mnist'

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def train(writer, epochs, transform, model):
    logging.info(f"Starting the training")
    train_data = datasets.MNIST(download=True, root=data_path, train=True, transform=transform)

    train_loader = DataLoader(dataset=train_data, batch_size=BATCH_SIZE, shuffle=True)

    model = model.to(device)
    optimizer = SGD(params=model.parameters(), lr=LEARNING_RATE)
    loss_fn = CrossEntropyLoss()

    loss = None
    n_iter = 0
    for epoch in range(epochs):
        for x, y in train_loader:
            n_iter += 1
            x = x.to(device)
            y = y.to(device)
            loss = loss_fn(model(x), y)
            writer.add_scalar('Loss', loss, n_iter)
            
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        logging.info(f"Epoch {epoch + 1}, loss {loss}")

    torch.save(model.state_dict(), model_path)


def test(writer, transform, model):
    logging.info('Running the test loop')
    model = model.to(device)
    model.load_state_dict(torch.load(model_path))
    test_data = datasets.MNIST(download=True, root='~/data/torchvision/mnist', train=False, transform=transform)
    test_loader = DataLoader(dataset=test_data, batch_size=BATCH_SIZE, shuffle=False)
    correct = 0
    total = 0

    for x, y in test_loader:
        x = x.to(device)
        y = y.to(device)
        total += x.shape[0]
        y_prime_prob = model(x)
        _, y_prime = torch.max(y_prime_prob, 1)
        correct += torch.sum(y_prime == y)
    logging.info(f"{correct}/{total}, accuracy{correct/total}")

def visualize(writer, model):
    model = model.to(device)
    model.load_state_dict(torch.load(model_path))
    zero_conv_data = model.features[0]
    first_conv_data = model.features[1]
    
    def normalize(x):
        x_min, x_max = x.min(), x.max()
        return (x - x_min) / (x_max - x_min)
    

    weights_0 = zero_conv_data.weight.data
    weights_1 = first_conv_data.weight.data

    # Normalize weights for better visualization
    weights_norm_0 = normalize(weights_0)
    weights_norm_1 = normalize(weights_1)

    
    grid_0 = torchvision.utils.make_grid(weights_norm_0, nrow=1, padding=1, normalize=True)
    grid_1 = torchvision.utils.make_grid(weights_norm_1, nrow=3, padding=1, normalize=True)
    
    
    writer.add_image('zero_conv_layer_weights', grid_0, 0)
    writer.add_image('first_conv_layer_weights', grid_1, 0)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO)
    parser = argparse.ArgumentParser(description='Process MNIST')
    parser.add_argument('--train', action=argparse.BooleanOptionalAction, required=False, default=False)
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--name', required=True)
    parser.add_argument('-v', '--visualize', action=argparse.BooleanOptionalAction, required=False, default=True)
    args = parser.parse_args()
    logging.info(f'Using device: {device}')
    writer = SummaryWriter(f'runs/{args.name}')

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, ), (0.5,))
    ])

    model = CustomResNet()
    if args.train:
        train(writer, args.epochs, transform, model)
    test(writer, transform, model)
    if args.visualize:
        visualize(writer, model)
    writer.close()
