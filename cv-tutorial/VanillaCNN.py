import torch
from torch import nn
ResNet = torch.hub.load('pytorch/vision:v0.10.0', 'resnet18', pretrained=True)

VanillaCNN = nn.Sequential(
    nn.Conv2d(in_channels=1, out_channels=3, kernel_size=5, padding=2, stride=1),
    nn.MaxPool2d(stride=2, kernel_size=2),
    nn.ReLU(),
    nn.Conv2d(in_channels=3, out_channels=64, kernel_size=5, padding=2, stride=1),
    nn.MaxPool2d(stride=2, kernel_size=2),
    nn.ReLU(),
    nn.Flatten(),
    nn.Linear(in_features=64*7*7, out_features=10,bias=True)
)

class VanillaCNNClass(nn.Module):

    def __init__(self):
        super().__init__()
        self.layer1 = nn.Conv2d(in_channels=1, out_channels=3, kernel_size=5, padding=2, stride=1)
        self.max_pool1 = nn.MaxPool2d(stride=2, kernel_size=2)
        self.layer2 = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=15, padding=2, stride=1)
        self.max_pool2 = nn.MaxPool2d(stride=2, kernel_size=2)
        self.linear = nn.Linear(in_features=32*2*2, out_features=10,bias=True)
        self.flatten = nn.Flatten()
        self.relu = nn.ReLU()
        self.features = [self.layer1, self.layer2]

    def forward(self, x):
        x = self.layer1(x)
        x = self.max_pool1(x)
        x = self.relu(x)
        cnn_out_1 = x
        cnn_out_2 = self.flatten(self.relu(self.max_pool2(self.layer2(cnn_out_1))))
        return self.linear(cnn_out_2)

class CustomResNet(nn.Module):

    def __init__(self):
        super().__init__()
        self.resnet = ResNet
        self.resnet.fc = nn.Linear(in_features=512, out_features=10, bias=True)

    def forward(self, x):
        x = nn.functional.interpolate(x, size=(64, 64))
        x = x.repeat(1, 3, 1, 1)
        x = self.resnet(x)
        return x
