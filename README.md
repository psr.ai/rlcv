# WELCOME TO THE HOME OF BLENDING COMPUTER VISION AND REINFORCEMENT LEARNING

We use a reinforcement learning and computer vision to solve real world problems. As a part of our learning together videos, we explore the problem sets and recent research papers together and implement them live on our twitch channel. Please feel free to reach out to the collaborators:

- [Prabhjot Singh Rai](https://www.linkedin.com/in/psr-ai/)
- [Hardik Uppal](https://www.linkedin.com/in/hardikuppal/)

## Our Schedule

We are currently live on [Twitch](https://www.twitch.tv/psr_ai) every Sunday at 1:00 PM CST (you can view the updated schedule [here](https://www.twitch.tv/psr_ai/schedule)). We also have a podcast where we discuss the recent research papers and the problem sets we are working on. Please have a look at our schedule here and our old videos on our [YouTube Playlist](https://www.youtube.com/playlist?list=PLYp61UYruOx6OYbr25facFjcGNhgd5-db).

## Folders

- cv-tutorial: This folder contains the tutorials on computer vision. Currently contains Vanilla CNN and ResNet.
- rl-tutorial: This folder contains the tutorials on reinforcement learning.
